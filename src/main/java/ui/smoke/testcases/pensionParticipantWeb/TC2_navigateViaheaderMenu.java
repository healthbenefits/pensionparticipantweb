package ui.smoke.testcases.pensionParticipantWeb;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.isElementExisting;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;

import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.pensionParticipantweb_Pages.HeaderMenuPage;
import pages.pensionParticipantweb_Pages.HomePage;
import pages.pensionParticipantweb_Pages.LoginManagementPage;
import pages.pensionParticipantweb_Pages.MyStatementPage;
import pages.pensionParticipantweb_Pages.PlanInformationPage;
import verify.SoftAssertions;
public class TC2_navigateViaheaderMenu {

	@Test(enabled = true, priority = 1)
	public void verifyPlanInformationHeaderMenu() throws Exception {
		try {
			test = reports.createTest("Navigation via header menu : Plan Information");
			test.assignCategory("smoke");
			driver.switchTo().defaultContent();
			HomePage homePage = new HomePage();
			homePage.switchFrame(HomePage.content_frame);
			SoftAssertions.assertTrue(isElementExisting(driver, HomePage.welcomeMessage_text, 5),"User has reached a home page", test);
			driver.switchTo().defaultContent();
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			homePage.switchFrame(HomePage.header_frame);
			headerMenuPage.navigateViaMenu(HeaderMenuPage.planInfo_button);
			driver.switchTo().defaultContent();
			PlanInformationPage planInfoPage = new PlanInformationPage();
			homePage.switchFrame(HomePage.content_frame);
			SoftAssertions.assertTrue(isElementExisting(driver, PlanInformationPage.planInfo_header, 5),"User has reached a Plan information page", test);
			
			planInfoPage.verifyRetirementBenefits();
			
			// Verify pdf file;
		}
		catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Navigation to plan information failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Navigation to plan information failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 2)
	public void verifyMyStatementHeaderMenu() throws Exception {
		try {
			test = reports.createTest("Navigation via header menu : My Statement");
			test.assignCategory("smoke");
			driver.switchTo().defaultContent();
			HomePage homePage = new HomePage();
			homePage.switchFrame(HomePage.header_frame);
			
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			headerMenuPage.navigateViaMenu(HeaderMenuPage.home_button);
			
			driver.switchTo().defaultContent();
			homePage.switchFrame(HomePage.content_frame);
			SoftAssertions.assertTrue(isElementExisting(driver, HomePage.welcomeMessage_text, 5),"User has reached a home page", test);
			driver.switchTo().defaultContent();
			homePage.switchFrame(HomePage.header_frame);
		headerMenuPage.navigateViaMenu(HeaderMenuPage.myStatement_button);
			driver.switchTo().defaultContent();
		MyStatementPage myStatementPage = new MyStatementPage();
			homePage.switchFrame(HomePage.content_frame);
			SoftAssertions.assertTrue(isElementExisting(driver, MyStatementPage.myStatement_header, 5),"User has reached a My statement page", test);
			
		}
		catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Navigation to my statement failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Navigation to my statement failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 3)
	public void verifyLoginManagementHeaderMenu() throws Exception {
		try {
			test = reports.createTest("Navigation via header menu : login Management");
			test.assignCategory("smoke");
			driver.switchTo().defaultContent();
			HomePage homePage = new HomePage();
			homePage.switchFrame(HomePage.header_frame);
			
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			headerMenuPage.navigateViaMenu(HeaderMenuPage.home_button);
			driver.switchTo().defaultContent();
			homePage.switchFrame(HomePage.content_frame);
			SoftAssertions.assertTrue(isElementExisting(driver, HomePage.welcomeMessage_text, 5),"User has reached a home page", test);
			driver.switchTo().defaultContent();
			homePage.switchFrame(HomePage.header_frame);
			headerMenuPage.navigateViaMenu(HeaderMenuPage.loginManagement_button);
			driver.switchTo().defaultContent();
			LoginManagementPage loginManagementPage = new LoginManagementPage();
			loginManagementPage.navigateToManagementWindow();
			
			SoftAssertions.assertTrue(isElementExisting(driver, LoginManagementPage.password_input, 5),"User has reached a Login Management password page", test);
			
			loginManagementPage.navigateToManagementPage(TC1_loginToPPW.PASSWORD);
			SoftAssertions.assertTrue(isElementExisting(driver, LoginManagementPage.contactInfo_header, 5),"User has reached a Login Management home page", test);
			SoftAssertions.assertTrue(isElementExisting(driver, LoginManagementPage.update_button, 5),"User has reached a Login Management home page", test);
			
			loginManagementPage.SwitchTParentWindow();
			
			driver.switchTo().defaultContent();
			homePage.switchFrame(HomePage.header_frame);
			SoftAssertions.assertTrue(isElementExisting(driver, HeaderMenuPage.home_button, 5),"User has reached a base window for further operations", test);
			
		}
		catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Navigation to Login Management failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Navigation to Login Management failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
}
}
