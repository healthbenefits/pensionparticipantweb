package ui.smoke.testcases.pensionParticipantWeb;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.isElementExisting;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;

import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.pensionParticipantweb_Pages.HomePage;
import pages.pensionParticipantweb_Pages.LoginPage;
import pages.pensionParticipantweb_Pages.LogoutPage;
import pages.pensionParticipantweb_Pages.SideMenuPage;
import verify.SoftAssertions;

public class TC4_logoutFromPPW {
	@Test(enabled = true, priority = 4)
	public void logoutFromPPW() throws Exception {
		try {
			test = reports.createTest("Logout from Pension participant Web");
			test.assignCategory("smoke");
			driver.switchTo().defaultContent();
			driver.switchTo().defaultContent();
			HomePage homePage = new HomePage();
			homePage.switchFrame(HomePage.sideMenu_frame);
			
			SideMenuPage sideMenuPage = new SideMenuPage();
			sideMenuPage.navigateViaMenu(SideMenuPage.logout_button);
			driver.switchTo().defaultContent();
			driver.switchTo().defaultContent();
			LogoutPage logoutPage = new LogoutPage();
			homePage.switchFrame(HomePage.content_frame);
			SoftAssertions.assertTrue(isElementExisting(driver, LogoutPage.logout_link, 5),"User has reached a log out page", test);
			logoutPage.performLogout();
			
			LoginPage loginPage = new LoginPage();
			SoftAssertions.assertTrue(isElementExisting(driver, LoginPage.login_button, 5),"User  successfully logged out of the application", test);
			
		}
		catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Logging out failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Logging out failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
}
