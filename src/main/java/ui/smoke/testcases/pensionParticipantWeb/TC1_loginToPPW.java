package ui.smoke.testcases.pensionParticipantWeb;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.initWebDriver;
import static driverfactory.Driver.isElementExisting;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;

import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.pensionParticipantweb_Pages.AcceptAgreementPage;
import pages.pensionParticipantweb_Pages.HomePage;
import pages.pensionParticipantweb_Pages.LoginPage;
import pages.pensionParticipantweb_Pages.MFAPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class TC1_loginToPPW extends InitTests {
	
	public  String MFAEmailId = "";
	public  String MFAEmailPassword = "";
	
	
	public TC1_loginToPPW(String appName) {
		super(appName);
	}
	
	@Test(enabled = true, priority = 1)
	public void loginToAdminWebUsingMFA() throws Exception {
		try {
			
			props.load(input);
			MFAEmailId = props.getProperty("MFAEmailId");
			MFAEmailPassword = props.getProperty("MFAPassword");
			System.out.println(MFAEmailId+">>>"+MFAEmailPassword);
		test = reports.createTest("Logging in to pension participant web with MFA");
		test.assignCategory("smoke");
		
		TC1_loginToPPW pensionParticipantWeb = new TC1_loginToPPW("PensionParticipantWeb");

		System.out.println("Logging in to application using MFA");
//		String url,String browserType,String browserVersion,String platform,String executionEnv,ExtentTest test,String nodeUrl
		initWebDriver(BASEURL,"CHROME", "latest", "", "local", test, "");
		System.out.println("BaseURL is: " + BASEURL);

		LoginPage loginPage = new LoginPage();
		SoftAssertions.assertTrue(isElementExisting(driver, LoginPage.username_input, 5),"User has reached a login page", test);
		loginPage.loginToapp(USERNAME, PASSWORD);
		
		MFAPage mfaPage = new MFAPage();
		mfaPage.enterMFA(MFAEmailId, MFAEmailPassword);
		Thread.sleep(30000);
		AcceptAgreementPage acceptAgreementPage = new AcceptAgreementPage();
		SoftAssertions.assertTrue(isElementExisting(driver, AcceptAgreementPage.iAgree_button, 5),"User has reached a agreement page", test);
		acceptAgreementPage.acceptAgreement();
		
		HomePage homePage = new HomePage();
		homePage.switchFrame(HomePage.content_frame);
		SoftAssertions.assertTrue(isElementExisting(driver, HomePage.welcomeMessage_text, 5),"User has reached a home page", test);
		
		}
		catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("MFA Login to ppw failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("MFA Login to ppw failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
		
	
	}
}
