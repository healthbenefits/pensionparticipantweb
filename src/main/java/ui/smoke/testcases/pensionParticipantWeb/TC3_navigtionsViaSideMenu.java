package ui.smoke.testcases.pensionParticipantWeb;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.isElementExisting;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;

import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.pensionParticipantweb_Pages.HomePage;
import pages.pensionParticipantweb_Pages.InformationAboutYouPage;
import pages.pensionParticipantweb_Pages.MyBeneficiariesPage;
import pages.pensionParticipantweb_Pages.SideMenuPage;
import verify.SoftAssertions;

public class TC3_navigtionsViaSideMenu {

	@Test(enabled = true, priority = 1)
	public void verifyInformationAboutYouSideMenu() throws Exception {
		try {
			test = reports.createTest("Navigation via side menu : Information About You");
			test.assignCategory("smoke");
			driver.switchTo().defaultContent();
			driver.switchTo().defaultContent();
			HomePage homePage = new HomePage();
//			homePage.switchFrame(HomePage.sideMenu_frame);
			driver.switchTo().frame("datamenu");
			
			SideMenuPage sideMenuPage = new SideMenuPage();
			sideMenuPage.navigateViaMenu(SideMenuPage.informationAboutYou_button);
			driver.switchTo().defaultContent();
			driver.switchTo().defaultContent();
			InformationAboutYouPage infoAboutYouPage = new InformationAboutYouPage();
			homePage.switchFrame(HomePage.content_frame);
			SoftAssertions.assertTrue(isElementExisting(driver, InformationAboutYouPage.employeeInformation_header, 5),"User has reached a Information about you page", test);
			
			
		}
		catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Navigation to Information About You failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Navigation to Information About You failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 2)
	public void verifyMyBeneficiariesSideMenu() throws Exception {
		try {
			test = reports.createTest("Navigation via side menu : my Beneficiaries");
			test.assignCategory("smoke");
			driver.switchTo().defaultContent();
			driver.switchTo().defaultContent();
			HomePage homePage = new HomePage();
			homePage.switchFrame(HomePage.sideMenu_frame);
			
			SideMenuPage sideMenuPage = new SideMenuPage();
			sideMenuPage.navigateViaMenu(SideMenuPage.beneficiaries_button);
			driver.switchTo().defaultContent();
			driver.switchTo().defaultContent();
			MyBeneficiariesPage myBeneficiariesPage = new MyBeneficiariesPage();
			homePage.switchFrame(HomePage.content_frame);
			SoftAssertions.assertTrue(isElementExisting(driver, MyBeneficiariesPage.myBeneficiaries_header, 5),"User has reached a My beneficiaries page", test);
			
			myBeneficiariesPage.downloadForm();
			// pdf verification pending
		}
		catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Navigation to Information About You failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Navigation to Information About You failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	@Test(enabled = true, priority = 3)
	public void verifyCalculateRetirementSideMenu() throws Exception {
		try {
			test = reports.createTest("Navigation via side menu : Calculate Retirement");
			test.assignCategory("smoke");
			//Module is broken thus can not be automated
		}
		catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Navigation to Calculte retirement failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Navigation to Calculte retirement failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 4)
	public void verifyPreviewCalculateRetirementSideMenu() throws Exception {
		try {
			test = reports.createTest("Navigation via side menu : Preview Retirement calculations");
			test.assignCategory("smoke");
			//Dependent on above module thus can not be automated
		}
		catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Navigation to Preview calculation retirement failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Navigation to Preview calculation retirement failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
}
