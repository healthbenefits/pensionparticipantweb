package pages.pensionParticipantweb_Pages;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.clickElement;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HeaderMenuPage {

	@FindBy(id = "N1001")
	public static WebElement home_button;
	
	@FindBy(id = "N1002")
	public static WebElement planInfo_button;
	
	@FindBy(id = "N1008")
	public static WebElement myStatement_button;

	@FindBy(id = "N1007")
	public static WebElement loginManagement_button;
	

	
	public HeaderMenuPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void navigateViaMenu(WebElement e ) {
		clickElement(e);
		
	}
}
