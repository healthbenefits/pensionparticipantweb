package pages.pensionParticipantweb_Pages;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.clickElement;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MyBeneficiariesPage {

	@FindBy(xpath = "//h2[contains(text(), 'Your Pre-Retirement Death Beneficiary(ies)')]")
	public static WebElement myBeneficiaries_header;
	
	@FindBy(xpath = "//b/a[contains(text(), 'here')]")
	public static WebElement beneficiariesCopy_link;
	
	public MyBeneficiariesPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void downloadForm() {
		clickElement(beneficiariesCopy_link);
	}
}
