package pages.pensionParticipantweb_Pages;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.switchToWindow;

import java.util.HashSet;
import java.util.Set;

import static driverfactory.Driver.setInput;
import static driverfactory.Driver.clickElement;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginManagementPage {

	@FindBy(id = "password")
	public static WebElement password_input;

	@FindBy(xpath = "//button[contains(text(), 'Continue')]")
	public static WebElement continue_button;

	@FindBy(xpath = "//h2[contains(text(), 'Contact Information')]")
	public static WebElement contactInfo_header;

	@FindBy(xpath = "//button[contains(text(), 'Update')]")
	public static WebElement update_button;

	public String baseHandle;

	public LoginManagementPage() {
		PageFactory.initElements(driver, this);
	}

	public void navigateToManagementWindow() {
		baseHandle = driver.getWindowHandle();
		baseHandle = driver.getWindowHandle();
		((JavascriptExecutor)driver).executeScript("window.open()");

		Set<String> tabs = new HashSet<String> ();
	
		for(int i = 0; i<5; i++) {
			tabs =driver.getWindowHandles();
			System.out.println(i+">>> number of tabs");
			if(tabs.size()==2) {
				System.out.println("breaking");
				break;
			}
			else {
				System.out.println("delay of 2 sec");
				delay(5000);
			}
		}
		for(String tab : tabs) {
			
			if(!(tab.equals(baseHandle))) {
				System.out.println(baseHandle+"   old handle");
				System.out.println(tab+"       new window handle");
				driver.switchTo().window(tab);
			}
			
		}

		driver.get("https://auth-qa.mercer.com/QACALC2/security-settings?uniqueId=2430");
		
		
	}

	public void navigateToManagementPage(String password) {
		// TODO Auto-generated method stub

		setInput(password_input, password);
		clickElement(continue_button);

	}

	public void SwitchTParentWindow() {

		
		System.out.println("Window closed and now going back to old window");
		switchToWindow("Base Retirement Web");
		driver.switchTo().window(baseHandle);
	}
}
