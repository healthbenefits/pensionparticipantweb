package pages.pensionParticipantweb_Pages;

import static driverfactory.Driver.driver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InformationAboutYouPage {

	@FindBy(xpath = "//span[contains(text(), 'Employee Information')]")
	public static WebElement employeeInformation_header;
	
	public InformationAboutYouPage() {
		PageFactory.initElements(driver, this);
	}
}
