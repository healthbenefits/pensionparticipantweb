package pages.pensionParticipantweb_Pages;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.clickElement;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AcceptAgreementPage {
	
	@FindBy(xpath = "//input[@value = 'I Agree']")
	public static WebElement iAgree_button;
	
	public AcceptAgreementPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void acceptAgreement() {
		clickElement(iAgree_button);
	}
}
