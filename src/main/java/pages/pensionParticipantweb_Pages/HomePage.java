package pages.pensionParticipantweb_Pages;

import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {


	@FindBy(xpath = "//b[contains(text(), 'Welcome Raymond Sawyer!')]")
	public static WebElement welcomeMessage_text;
	
	@FindBy(xpath = "//frame[@name = 'content']/preceding-sibling::frame[contains(@name , 'datamenu')]")
	public static WebElement sideMenu_frame;
	
	@FindBy(xpath = "//frame[@name = 'content']")
	public static WebElement content_frame;
	
	@FindBy(xpath = "//frame[@name = 'header']")
	public static WebElement header_frame;
	
	
	public HomePage () {
		PageFactory.initElements(driver, this);
	}
	
	public void switchFrame (WebElement frame) {
		driver.switchTo().frame(frame);
	}
}
