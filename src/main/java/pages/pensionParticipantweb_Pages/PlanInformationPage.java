package pages.pensionParticipantweb_Pages;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.clickElement;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PlanInformationPage {
	@FindBy(xpath = "//font[contains(text(), 'Summary Plan Description')]")
	public static WebElement planInfo_header;
	
	@FindBy(xpath = "//u[contains(text(), '2012 Your Retirement Benefits Book (YRB)')]/parent::font/parent::a")
	public static WebElement retirementBenefits_link;
	
	@FindBy(id = "plugin")
	public static WebElement embedded_plugin;
	
	public PlanInformationPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void verifyRetirementBenefits () {
		clickElement(PlanInformationPage.retirementBenefits_link);
	}
}
