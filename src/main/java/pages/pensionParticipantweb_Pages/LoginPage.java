package pages.pensionParticipantweb_Pages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.setInput;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

	
	@FindBy(xpath = "//input[@name='username']")
	public static WebElement username_input;
	
	@FindBy(xpath = "//input[@name = 'password']")
	public static WebElement password_input;
	
	@FindBy(xpath = "//button[@type = 'submit']")
	public static WebElement login_button;
	
	@FindBy(xpath = "//label[contains(text(), 'Email ni*****@mercer.com')]")
	public static WebElement mfaVerificationEmail_radiobutton;
	
	@FindBy(xpath = "//button[contains(text(), 'Continue')]")
	public static WebElement continue_button;
	
	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void loginToapp(String username, String password) {
		setInput(username_input, username);
		setInput(password_input, password);
		clickElement(login_button);		
		clickElement(mfaVerificationEmail_radiobutton);
		clickElement(continue_button);
	}
}
