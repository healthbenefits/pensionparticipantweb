package pages.pensionParticipantweb_Pages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SideMenuPage {
	
	@FindBy(xpath = "//a[contains(text(), 'Information About You')]")
	public static WebElement informationAboutYou_button;
	
	@FindBy(xpath = "//a[contains(text(), 'My Beneficiaries - NEW!')]")
	public static WebElement beneficiaries_button;
	
	@FindBy(xpath = "//a[contains(text(), 'Calculate Retirement Estimate')]")
	public static WebElement calculateEstimates_button;

	@FindBy(xpath = "//a[contains(text(), 'Review Previous Retirement Estimates')]")
	public static WebElement reviewEstimates_button;
	
	@FindBy(xpath = "//a[contains(text(), 'Logout')]")
	public static WebElement logout_button;
	
	public SideMenuPage() {
		PageFactory.initElements(driver, this);
	}
	
	
	public void navigateViaMenu(WebElement e ) {
		
		clickElement(e);
	}


	
}
