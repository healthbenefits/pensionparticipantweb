package pages.pensionParticipantweb_Pages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.clickElementUsingJavaScript;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.getElementText;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;

import java.awt.AWTException;
import java.util.HashSet;
import java.util.Set;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MFAPage {
	@FindBy(xpath = "//label[contains(text(), 'Email ni*****@mercer.com')]")
	public static WebElement mfaVerificationEmail_radiobutton;
	
	@FindBy(id = "username")
	public static WebElement outlookEmailID;
	
	@FindBy(id = "password")
	public static WebElement outlookPassword;
	
	@FindBy(xpath = "//span[text()='Unread']")
	public static WebElement unreadMenu;
	
	@FindBy(xpath = "//span[contains(text(), 'MFA Verification')]")
	public static WebElement mfaEmail;
	
	@FindBy(xpath = "//div[@id = 'Item.MessageUniqueBody']/div/div")
	public static WebElement emailContent;
	
	@FindBy(id = "verificationCode")
	public static WebElement mfaCode_input;
	
	@FindBy(xpath = "//button[contains(text(), 'Continue')]")
	public static WebElement continue_button;
	
	static String baseHandle;
	public MFAPage() {
		PageFactory.initElements(driver, this);
	}

	
	public void enterMFA(String username, String password) throws AWTException {

		baseHandle = driver.getWindowHandle();
		((JavascriptExecutor)driver).executeScript("window.open()");

		Set<String> tabs = new HashSet<String> ();
	
		for(int i = 0; i<5; i++) {
			tabs =driver.getWindowHandles();
			System.out.println(i+">>> number of tabs");
			if(tabs.size()==2) {
				System.out.println("breaking");
				break;
			}
			else {
				System.out.println("delay of 2 sec");
				delay(5000);
			}
		}
		for(String tab : tabs) {
			
			if(!(tab.equals(baseHandle))) {
				System.out.println(baseHandle+"   old handle");
				System.out.println(tab+"       new window handle");
				driver.switchTo().window(tab);
			}
			
		}

		driver.get("https://apac1mail.mmc.com/OWA/");
		waitForElementToDisplay(outlookEmailID);
		setInput(outlookEmailID,username);
		setInput(outlookPassword,password+Keys.ENTER);
		waitForElementToDisplay(unreadMenu);
		clickElementUsingJavaScript(driver, unreadMenu);
		clickElementUsingJavaScript(driver, mfaEmail);
		waitForElementToDisplay(emailContent);
		String content = getElementText(emailContent);
		System.out.println(content+"        "+(content.split(":"))[1].trim());
		driver.switchTo().window(baseHandle);
		setInput(mfaCode_input, (content.split(":"))[1].trim());
		clickElement(continue_button);

	}
}
