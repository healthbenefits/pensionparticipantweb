package pages.pensionParticipantweb_Pages;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.clickElement;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import org.openqa.selenium.support.PageFactory;

public class LogoutPage {
	
	
	@FindBy(xpath = "//input[@value = ' LOGOUT ']")
	public static WebElement logout_link;
	
	public LogoutPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void performLogout() {
		clickElement(logout_link);
	}
}
