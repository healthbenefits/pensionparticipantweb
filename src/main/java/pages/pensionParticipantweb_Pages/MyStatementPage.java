package pages.pensionParticipantweb_Pages;

import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MyStatementPage {
	@FindBy(xpath = "//font[contains(text(), 'My Statement')]")
	public static WebElement myStatement_header;
	
	public MyStatementPage() {
		PageFactory.initElements(driver, this);
	}
}
